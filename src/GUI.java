import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.LinkedList;

public class GUI extends JFrame implements ActionListener {
    Connection connection=new Connection();
JButton add_dbc=new JButton("Upload new dbc");
JButton get_dbc=new JButton("Get dbc");
JButton get_telemetry_data=new JButton("Get Telemetry data");
JRadioButton dbc_all=new JRadioButton("DBC all");
JRadioButton dbc_fetch=new JRadioButton("DBC fetch");
ButtonGroup buttonGroup=new ButtonGroup();
JTextArea server_response=new JTextArea();
JScrollPane scrollPane=new JScrollPane(server_response);
JPanel panel=new JPanel();
JButton execute=new JButton("Execute!");

JLabel key1=new JLabel("default");
JLabel key2=new JLabel("default");
JLabel key3=new JLabel("default");
JLabel key4=new JLabel("default");

JTextField textField1=new JTextField(7);
JTextField textField2=new JTextField(7);
JTextField textField3=new JTextField(7);
JTextField textField4=new JTextField(7);


public GUI(){
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    setTitle("Telemetry app");
    setResizable(true);
    setPreferredSize(new Dimension(500,500));
    panel.setPreferredSize(new Dimension(500,500));
    scrollPane.setPreferredSize(new Dimension(200,200));

    add_dbc.addActionListener(this);
    get_dbc.addActionListener(this);
    get_telemetry_data.addActionListener(this);

    key1.setEnabled(false);
    key1.setEnabled(false);
    key1.setEnabled(false);
    key1.setEnabled(false);

    textField1.setEditable(false);
    textField2.setEditable(false);
    textField3.setEditable(false);
    textField4.setEditable(false);

    panel.add(add_dbc);
    panel.add(get_dbc);
    panel.add(get_telemetry_data);
    panel.add(key1);
    panel.add(key2);
    panel.add(key3);
    panel.add(key4);
    panel.add(textField1);
    panel.add(textField2);
    panel.add(textField3);
    panel.add(textField4);
    panel.add(scrollPane);

    buttonGroup.add(dbc_all);
    buttonGroup.add(dbc_fetch);
    panel.add(dbc_all);
    panel.add(dbc_fetch);
    //panel.add(scrollPane);

    pack();
    setContentPane(panel);
    setVisible(true);
}

    @Override
    public void actionPerformed(ActionEvent e) {
    Object zrodlo=e.getSource();
    if(zrodlo==add_dbc){
        key1.setText("name");
        key2.setText("version");
        key3.setText("payload");
        textField1.setEditable(true);
        textField2.setEditable(true);
        textField3.setEditable(true);
    }
    if(zrodlo==get_dbc){
       key1.setText("name");
       key2.setText("version");
       key3.setText("timestamp");
       key4.setText("payload");
       if(dbc_all.isSelected()){
        connection.get("http://localhost:8092/dbc/all");
        LinkedList list=connection.parse(connection.getResponse());
        Iterator iter=list.iterator();
        while(iter.hasNext()){
            String id=iter.next().toString();
            String version=iter.next().toString();
            int timestamp=(Integer)iter.next();
            String payload=iter.next().toString();
            server_response.append("name: "+id+" version: "+version+" timestamp: "+timestamp+" payload: "+payload+"\n");
        }
       }
       else if(dbc_fetch.isSelected()){
           key1.setText("name");
           key2.setText("version");
           key3.setText("timestamp");
           key4.setText("payload");
           connection.get("http://localhost:8092/dbc/fetch");
           LinkedList list=connection.parse(connection.getResponse());
           textField1.setText(list.get(0).toString());
           textField2.setText(list.get(1).toString());
           textField3.setText(list.get(2)+"");
           textField4.setText(list.get(3).toString());
       }
       else
           JOptionPane.showMessageDialog(this,"Musisz wybrać rodzaj requesta!","Błąd!",JOptionPane.ERROR_MESSAGE);
    }

    }

    public static void main(String[] args) {
        new GUI();
    }
}

